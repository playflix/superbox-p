<?php

$url = "https://sg-sg-sg.astro.com.my:9443/ctap/r1.6.0/devices/me/playsessions?channelId=601";
$token = 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE2MzUwODI2MDUsInN1YiI6Ijk1OTYzNTI3IiwiYXVkIjoiaXZwLnNlc3Npb25ndWFyZCIsImV4cCI6MTYzNTA5MzQwNSwic2Vzc2lvbl9kYXRhIjp7InNlc3Npb24iOnsiY29tbXVuaXR5IjoiTWFsYXlzaWEgTGl2ZSIsImJ1c1VuaXRJZCI6IkFTVFJPIiwic2NvcGUiOiJicm93c2UgcGxheWJhY2siLCJ1cElkIjoiOTU5NjM1MjdfMCIsImRldklkIjoiOTU5NjM1MjcuMjNiMThhMzgtNDAzZS00NzgyLThiY2UtYjk2Y2U2YmQ1MTZkIiwicmVnaW9uIjoiQ0VOVFJBTCIsImNtZGNSZWdpb24iOiI4MDcwMCIsImRldmljZUZlYXR1cmVzIjpbIkFCUiIsIlBFUlNPTkFMLUNPTVBVVEVSIiwiVU5NQU5BR0VEIiwiREFTSCIsIldWLURSTSIsIlNlY29uZFNjcmVlbiJdLCJjbWRjRGV2aWNlVHlwZSI6IlBDIiwiZGV2aWNlVHlwZSI6IkNPTVBBTklPTiIsInRlbmFudCI6ImsiLCJzb2x1dGlvbklkIjoiayIsInRlbmFudElkIjoiQVNUUk8iLCJndWVzdE1vZGUiOmZhbHNlLCJoaElkIjoiOTU5NjM1MjcifSwiaGhIYXNoIjo0NCwiY29tbXVuaXR5IjoiTWFsYXlzaWEgTGl2ZSJ9LCJzY29wZSI6ImJyb3dzZSBwbGF5YmFjayIsInRva2VuX3R5cGUiOiJhY2Nlc3NfdG9rZW4iLCJzc2FfanRpIjoiYnJvd3NlciIsImNsaWVudF9pZCI6ImJyb3dzZXIiLCJqdGkiOiIxZWFmOGM4ZS03N2Q0LTQ2MmQtOTFlNy03MDFkZjAyOTZlN2MifQ.xPAfWMqemUniB8Qnruz98W1G80Hd0bA8w0QAM7xoi8U';

$curl = curl_init($url);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_POST, true);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

$headers = array(
   "Host: sg-sg-sg.astro.com.my:9443",
   "Authorization: Bearer $token",
   "User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.71 Safari/537.36",
   "Content-Type: application/json",
   "Origin: https://astrogo.astro.com.my",
   "Referer: https://astrogo.astro.com.my/",
   "Content-Length: 0",
);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
//for debug only!
curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

$resp = curl_exec($curl);
curl_close($curl);
$result = json_decode($resp, true);
$mpdurl = $result['_links']['playUrl']['href'];
echo $mpdurl;
exit;


?>
